#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#======================================================================================#
#                                   WSDM                                                #
#======================================================================================#
"""WSDM_analysis_by_wetland_typo :  Wetland Soils Denitrification Model script to analyze denitrification rates by wetlands typology."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar, R. Cakir"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#

#import libraries
import WSDM_io
import WSDM_plots 
import WSDM_tools

import os
import glob
import time
import numpy as np
import scipy.interpolate
import shutil
import rasterio
import matplotlib.pyplot as plt
from datetime import datetime, date, time, timedelta
import pandas as pd

#########################
#  TO UPDATE
####################
runs_path=r"run_sample/Analysis"
param_path=os.path.join(runs_path,"inp","param_analysis.xml")
ease_lat_lon_path=os.path.join(runs_path,"inp","inp_EASE25v2_lon_lat.nc")

params = WSDM_io.readXML(param_path)

#########################
#  LOAD Data
####################


def extract_total_wetl_type(target_array,array_selection,type_wetl):
    temp=array_selection    
    temp[temp!=type_wetl]=0
    temp[temp==type_wetl]=1
    # np.unique(temp)

    target_array_select=target_array*temp
    sum_target_array_select=target_array_select.astype(np.int).sum()
    
    return sum_target_array_select


def cut_mask(target_array,params):
    rasterD = rasterio.open(params['world_watersheds'][0])
    bsn_msk=params['watershed_id'][0]
    bsn_msk = rasterD.read(1)
    bsn_msk[bsn_msk!=params['watershed_id'][0]]=0
    bsn_msk[bsn_msk==params['watershed_id'][0]]=1
    plt.imshow(bsn_msk)
# # Cut the region interest in the mask
    [LON_wise, LAT_wise] = WSDM_io.get_lon_lat_vectors('../data/SOIL/BULK.nc')
    [idx_lonmin_wise, idx_lonmax_wise, idx_latmin_wise, idx_latmax_wise]= WSDM_io.get_ROI_coord_indexs(LON_wise, LAT_wise, params['lonmin'][0], params['lonmax'][0],params['latmin'][0], params['latmax'][0])

    bsn_msk_select=bsn_msk[idx_latmax_wise:idx_latmin_wise,idx_lonmin_wise: idx_lonmax_wise]
    bsn_msk_select = bsn_msk_select.astype("float")
    bsn_msk_select[bsn_msk_select==255]=np.NaN
    
    target_array_new=target_array*bsn_msk_select
    plt.imshow(target_array_new)
    return target_array_new



if __name__ == "__main__":
########################
  # KOC and DENIT IN ROI 
########################
    data_year=pd.DataFrame(columns= ['Year','KOC_uq','Sum_annual_denit'])
    year=0
    list_year=('2011','2012','2013','2014','2015','2016','2017','2018', '2019')

data_analysis_dir=params['data_analysis'][0]
anc=np.load(params["data_anc"][0])
anc.files
KOC=anc["KOC_opt"]
type_wetland=np.unique(KOC)
print(type_wetland)

#########################
#  EXTRACT BY WETLANDS TYPES in the ROI YEARLY
#######################
for year in list_year:
    print(year)
    data_yr=[]
    data_yr=np.load(data_analysis_dir+"out_year/"+year+'_analysis.npz')
    denit_sum_yr=data_yr['denit_annual_sum']
    sum_denit_wetl=[]
    wetl_list=[]
    for i,wetl in enumerate(type_wetland):
        print("processing",wetl)
        
        if wetl != 0:
            wetl_list.append(wetl) 
            sum_denit_wetl.append(extract_total_wetl_type(np.nan_to_num(denit_sum_yr),np.nan_to_num(anc['KOC_opt']),wetl)) 
            
            print( "Year",year,"sum DNIT =",sum_denit_wetl[i-1])
    
    
    d= {'Year':year,'KOC_uq':wetl_list,'Sum_annual_denit':sum_denit_wetl}
    data_year= data_year.append(pd.DataFrame(d))
data_year['Year']=data_year.Year.astype(int)
data_year.to_csv(runs_path+'/wetland_denit_sum_annual_'+params['ROI_name'][0]+'.csv')
data_year.dtypes
plt.plot(data_year.Year,data_year.Sum_annual_denit)


###### update according to ROI#####
landuse=['Peatlands','Swamps','Freshwater Marsh','Wetlands complex','Brackish wetlands']
###### update according to ROI#####

for i,wetl in enumerate(wetl_list):
    print(i)
    select=data_year[(data_year.KOC_uq ==wetl_list[i])]
    plt.plot(select.Year,select.Sum_annual_denit)
    plt.legend(landuse)
    plt.title("Sum denit annual evolution by wetland types")

plt.savefig(runs_path +'/weltand_denit_sum_year_'+params['ROI_name'][0]+'.png',dpi=300, bbox_inches='tight')
    

#########################
#  EXTRACT BY WETLANDS TYPES in the ROI MONTHLY
####################

type_wetland=np.unique(KOC)


data_month=pd.DataFrame(columns= ['Month','KOC_uq','Sum_month_denit'])
month='Jan'
list_month=['Jan','Feb', 'Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
list_month_num=['01','02', '03', '04','05','06','07','08','09','10','11','12']

for month in list_month:
    print(month)
    data_mth=[]
    data_mth=np.load(data_analysis_dir+"out_month/"+month+'_analysis.npz')
    sum_denit_wetl=[]
    wetl_list=[]
    for i,wetl in enumerate(type_wetland):
        print("processing",wetl)
        
        if wetl != 0:
            wetl_list.append(wetl) 
            sum_denit_wetl.append(extract_total_wetl_type(np.nan_to_num(data_mth['Denit_sum']),np.nan_to_num(anc['KOC_opt']),wetl)) 
            print( "Month",month,"sum DNIT =",sum_denit_wetl[i-1])
     
    d= {'Month':month,'KOC_uq':wetl_list,'Sum_month_denit':sum_denit_wetl}
    data_month= data_month.append(pd.DataFrame(d))
    # data_month['Month']=data_month.Month.astype(int)
    
data_month.to_csv(runs_path+'/weltand_denit_sum_month_'+params['ROI_name'][0]+'.csv')
data_month.dtypes
plt.plot(data_month.Month,data_month.Sum_month_denit)



landuse=['Peatlands','Swamps','Freshwater Marsh','Wetlands complex','Brackish wetlands']
for i,wetl in enumerate(wetl_list):
    print(i)
    select=data_month[(data_month.KOC_uq ==wetl_list[i])]
    plt.plot(select.Month,select.Sum_month_denit)
    plt.legend(landuse)
    # plt.set_ylabel("kg N.month$^{-1}$)")
    plt.title("Interannual monthly evolution by wetland type")

plt.savefig(runs_path +'/weltand_denit_sum_month_'+params['ROI_name'][0]+'.png',dpi=300, bbox_inches='tight')
    

#########################
# apply watershed MASK
####################

mask_dnit=np.nan_to_num(cut_mask(denit_sum_yr,params))
plt.imshow(mask_dnit)
anc=np.load(params["data_anc"][0])
anc.files
KOC=anc["KOC_opt"]

KOC_mask=np.nan_to_num(cut_mask(KOC,params))


#########################
#  EXTRACT BY WETLANDS TYPES BY YEAR 
####################
type_wetland=np.unique(KOC_mask)

# Used fonction EXAMPLE
extract_total_wetl_type(mask_dnit,KOC,type_wetland[2])    

#loop for each WETLANDS

data_year=pd.DataFrame(columns= ['Year','KOC_uq','Sum_annual_denit'])
year=0
list_year=('2011','2012','2013','2014','2015','2016','2017','2018', '2019')

lon_new=anc['lon']
lat_new=anc['lat']

lon_new=lon_new[lon_new>74]
lat_new=lat_new[lat_new>20]

for year in list_year:
    print(year)
    data_yr=[]
    data_yr=np.load(data_analysis_dir+"out_year/"+year+'_analysis.npz')
   
    denit_sum_yr=data_yr['denit_annual_sum']
    data_to_plot=cut_mask(denit_sum_yr,params)
    qa_sum=np.quantile(data_to_plot ,0.99)
    data_to_plot2=data_to_plot[0:len(lat_new),0:len(lon_new)]
    WSDM_plots.make_map(lat_new,lon_new,data_to_plot2,'deni_sum_'+year+'_'+params['Watershed_name'][0],'Annual denitrification (kgN.ha$^{-1}$.yr$^{-1}$) for '+year,0.000,27)
    sum_denit_wetl=[]
    wetl_list=[]
    for i,wetl in enumerate(type_wetland):
        print("processing",wetl)
        
        if wetl != 0:
            wetl_list.append(wetl) 
            sum_denit_wetl.append(extract_total_wetl_type(np.nan_to_num(cut_mask(denit_sum_yr,params)),np.nan_to_num(cut_mask(anc['KOC_opt'],params)),wetl)) 
            
            print( "Year",year,"sum DNIT =",sum_denit_wetl[i-1] )
    
    
    d= {'Year':year,'KOC_uq':wetl_list,'Sum_annual_denit':sum_denit_wetl}
    data_year= data_year.append(pd.DataFrame(d))
data_year['Year']=data_year.Year.astype(int)
data_year.to_csv(runs_path+'/weltand_denit_sum_annual_'+params['Watershed_name'][0]+'.csv')
data_year.dtypes
plt.plot(data_year.Year,data_year.Sum_annual_denit)



landuse=['Peatlands','Freshwater Marsh','Wetlands complex', 'Brackish wetlands']
for i,wetl in enumerate(wetl_list):
    print(i)
    select=data_year[(data_year.KOC_uq ==wetl_list[i])]
    plt.plot(select.Year,select.Sum_annual_denit)
    plt.legend(landuse)
    plt.title("Sum denit annual evolution by wetland types")

plt.savefig('..\Analysis\graphs/wetland_denit_sum_annual.png',dpi=300, bbox_inches='tight')
    
plt.imshow(cut_mask(data_yr['denit'],params))
# plt.imshow(mask_dnit)
plt.colorbar()


data_month=np.load(data_analysis_dir+"out_month/"+month+'_analysis.npz')

#elements inside  ['Denit_tot', 'Denit_sum', 'Denit_mean', 'Denit_max']

denit_sum_month=data_month['Denit_sum'] #data_month.files ['arr_0', 'arr_1'] <=> denit_sum_yr, denit_avg_yr
KOC=data_anc["KOC_opt"] #data_anc1.files

#########################
# apply MASK
####################

mask_dnit=np.nan_to_num(cut_mask(data_month['Denit_sum'],params))

KOC_mask=np.nan_to_num(cut_mask(anc['KOC_opt'],params))



#########################
#  EXTRACT BY WETLANDS TYPES BY MONTH
####################
type_wetland=np.unique(KOC_mask)

# Used fonction EXAMPLE
extract_total_wetl_type(mask_dnit,KOC,type_wetland[2])    

#boucle for each WETLANDS

data_month=pd.DataFrame(columns= ['Month','KOC_uq','Sum_month_denit'])
month='Jan'
list_month=['Jan','Feb', 'Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
list_month_num=['01','02', '03', '04','05','06','07','08','09','10','11','12']

# lon_new=data_anc1['lon']

# lon_new=lon_new[lon_new<-46]

for month in list_month:
    print(month)
    data_mth=[]
    data_mth=np.load(data_analysis_dir+"out_month/"+month+'_analysis.npz')
    # data_to_plot=cut_mask(data_month['Denit_sum'],params)
    # data_to_plot2=data_to_plot[0:len(lat_new),0:len(lon_new)]
    # ge_denit_plots.make_map(lat_new,lon_new,data_to_plot2,'deni_sum_'+ month+'_'+params['Watershed_name'][0],'interannual monthly denitrification (kgN.ha$^{-1}$.month$^{-1}$) for '+ month ,0.000,27)
    sum_denit_wetl=[]
    wetl_list=[]
    for i,wetl in enumerate(type_wetland):
        print("processing",wetl)
        
        if wetl != 0:
            wetl_list.append(wetl) 
            sum_denit_wetl.append(extract_total_wetl_type(np.nan_to_num(cut_mask(data_mth['Denit_sum'],params)),np.nan_to_num(cut_mask(anc['KOC_opt'],params)),wetl)) 
            
            print( "Month",month,"sum DNIT =",sum_denit_wetl[i-1])
    
    
    d= {'Month':month,'KOC_uq':wetl_list,'Sum_month_denit':sum_denit_wetl}
    data_month= data_month.append(pd.DataFrame(d))
# data_month['Month']=data_month.Month.astype(int)
    
data_month.to_csv(runs_path+'/weltand_denit_sum_month_'+params['Watershed_name'][0]+'.csv')
data_month.dtypes
plt.plot(data_month.Month,data_month.Sum_month_denit)



landuse=['Peatlands','Freshwater Marsh','Wetlands complex','Brackish wetlands']
for i,wetl in enumerate(wetl_list):
    print(i)
    select=data_month[(data_month.KOC_uq ==wetl_list[i])]
    plt.plot(select.Month,select.Sum_month_denit)
    plt.legend(landuse)
    # plt.set_ylabel("kg N.month$^{-1}$)")
    plt.title("Interannual monthly evolution by wetland type")

plt.savefig(runs_path +'/weltand_denit_sum_annual_'+params['Watershed_name'][0]+'.png'),dpi=300, bbox_inches='tight')
    
plt.imshow(cut_mask(data_yr['arr_0'],params))
# plt.imshow(mask_dnit)
plt.colorbar()

