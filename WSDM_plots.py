#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#======================================================================================#
#                                   WSDM                                                #
#======================================================================================#
"""WSDM_plots: Wetland Soils Denitrification Model plots script."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from mpl_toolkits.basemap import Basemap

def make_forcplot(filename):
    """
    Parameters
    ----------
    filename : string
        Path to the input np file.

    Returns
    -------
    plot 

    """
    data=np.load(filename)
   # for key in data:
   #     print(key)
    ## plots
    plt.subplot(211)
    plt.imshow(data['Tsoil'], interpolation='none',cmap='jet')
    plt.title('Tsoil')
    plt.colorbar()
    
    plt.subplot(212)
    plt.imshow(data['SM'], interpolation='none',cmap='jet')
    plt.title('SM')
    plt.gcf().set_size_inches(6, 6)
    plt.colorbar()
   
    plt.show()    

def make_denitplot(filename):
    data=np.load(filename)
    make_map(data['lat'],data['lon'],data['R_NO3'],'R_NO3.png','R_NO3',0.01,data['R_NO3'].max())
    make_map(data['lat'],data['lon'],data['R_NO3'],'RNO3_act.png','RNO3_act',0.0,1.5)

    plt.subplot(211)
    plt.imshow(data['NO3_act'], interpolation='none',cmap='jet')
    plt.title('NO3_act')
    plt.gcf().set_size_inches(6, 6)
    plt.clim(0, 1.5)
    plt.colorbar()
   
    data=np.load(filename)
    plt.subplot(212)
    plt.imshow(data['R_NO3'], interpolation='none',cmap='jet')
    plt.title('R_NO3')
    plt.gcf().set_size_inches(6,6)
    plt.colorbar()
    plt.clim(0, 1.5)
   
    plt.show()   
    

def make_ancplot(filename):
    """
    plots for visualization of soil input data spatialized

    Parameters
    ----------
    filename : string
        Path to the input np file.

    Returns
    -------
    plots.

    """
    data=np.load(filename)
    for key in data:
        print(key)
    ## soil plots
    make_map(data['lat'],data['lon'],data['Bulk'],'bulk.png','Bulk_opt',0.001,0.1)

    plt.subplot(321)
    plt.imshow(data['Bulk'],cmap='gist_ncar')
    plt.title('Bulk')
    plt.colorbar()

    plt.subplot(322)
    plt.imshow(data['OrgC'],cmap='gist_ncar')
    plt.title('OrgC')
    plt.colorbar()

    plt.subplot(323)
    plt.imshow(data['TCEQ'],cmap='gist_ncar')
    plt.title('TCEQ')
    plt.colorbar()

    plt.subplot(324)
    plt.imshow(data['CNrt'],cmap='gist_ncar')
    plt.title('CNrt')
    plt.colorbar()

    plt.subplot(325)
    plt.imshow(data['CLPC'],cmap='gist_ncar')
    plt.title('CLPC')
    plt.colorbar()

    plt.subplot(326)
    plt.imshow(data['SMmax'],cmap='gist_ncar')
    plt.title('SMmax')
    plt.colorbar()

    plt.gcf().set_size_inches(6, 10)
    plt.show() 
    # KOC_plots
    plt.subplot(211)
    plt.imshow(data['KOC_opt'],cmap='jet')
    plt.title('KOC_opt')
    plt.colorbar()

    plt.subplot(212)
    plt.imshow(data['land_class'],cmap='nipy_spectral')
    plt.title('land_class')
    plt.gcf().set_size_inches(12, 12)
    plt.colorbar()

    plt.show() 

def make_ancmap(filename):
    """
    To plot variables
    Parameters
    ----------
    filename : string
        Path to the input np file.

    Returns
    -------
    plot.

    """
    data=np.load(filename)
    for key in data:
        print(key)
    ## soil plots
    make_map(data['lat'],data['lon'],data['KOC_opt'],'soil_koc.png','KOC_opt',0.001,0.1)
  
    
    
def make_map(lat,lon,DATA,outfilename,title,v_min,v_max):
    """
    creat a plot of a variable given 

    Parameters
    ----------
    lat : 1D array
        latitude vector.
    lon : 1D array
        longitudes vector.
    DATA : 2D array
        variable to be plot.
    outfilename : string
        name of the plot to be save.
    title : string
        title of the plot
    v_min : float
        minimum value of the scale.
    v_max : float
        maximun value of the range of the variable to be shown.

    Returns
    -------
    plot

    """
    plt.figure(figsize=(12,12))

    plt.title(title)
    lon=np.squeeze(lon)
    lat=np.squeeze(lat)
    ##global extention of datasets
    parallels = np.arange(-55.,83.,20.)
    meridians = np.arange(-180.,180.,20.)
    map= Basemap(projection='cyl', llcrnrlon=lon[0] ,llcrnrlat=lat[-1],urcrnrlon=lon[-1],urcrnrlat=lat[0], resolution = 'h')
    map.readshapefile('..\wetlands\wetlands_shape\wetlands','Wetlands',color='white',  linewidth=0.1)
    cmap=plt.cm.get_cmap('YlOrRd') ######for denitrification
    # cmap=plt.cm.get_cmap('Blues')##### for soil moisture
    # cmap=plt.cm.get_cmap('Set1') ###### for soil parameters
    cmap.set_under('slategray')
    grid_x,grid_y = np.meshgrid(np.squeeze(lon),np.squeeze(lat))
    im=map.pcolormesh(grid_x, grid_y, DATA, cmap=cmap, vmin=v_min, vmax=v_max)
   ## labels = [left,right,top,bottom]
    map.drawparallels(parallels,labels=[True,False,False,False], color='k', linewidth=0.05)
    map.drawmeridians(meridians,labels=[False,False,False,True], color='k', linewidth=0.05)
    ##map countries
    # map.drawcountries(linestyle="solid",color='k' , linewidth=0.03)
    map.drawcoastlines(linestyle="solid",color='gray' , linewidth=0.07)
    map.drawmapboundary(fill_color='azure')
    # map.drawrivers(linestyle="solid",color='darkblue' , linewidth=0.1)
    ## map watersheds
    # map.readshapefile('..\watersheds\wribasin_area','Watersheds',color='k' , linewidth=0.7)
    
    # map.readshapefile()
    im_ratio = DATA.shape[0]/DATA.shape[1] 
    plt.colorbar(im,fraction=0.046*im_ratio, pad=0.05)
    plt.savefig(outfilename, bbox_inches='tight',dpi=200)
    plt.show()

def average_plot(table,variable_name,path_out, ROI,color):
    """
    Fuction to plot the average with the error area

    Parameters
    ----------
    table : 1D array
        date.
    variable_name : string
        name of variable to be plot.
    path_out : string
        path of the plot to be save.
    ROI : string
        region of interest.
    color : string
       defition of plot color.

    Returns
    -------
    plot

    """
    from datetime import datetime 
    for i in range(0,len(data)): 
        table['DATES'][i]= datetime.strptime(str(table['Date'][i]),'%Y%m%d')

    if table.empty :
        print(ROI,' table is empty')
        
    else: 
        table['DATES'] = pd.to_datetime(table['DATES'])
              
        median_list=table.groupby(table['DATES'].dt.month)[variable_name].median()
        sem_list=table.groupby(table['DATES'].dt.month)[variable_name].sem()
      

        plt.plot(table['DATES'].dt.month.unique(), median_list,color=color)
        plt.fill_between(x=table['DATES'].dt.month.unique(),y1=median_list-sem_list,y2=median_list+sem_list, alpha=0.3,color=color)
           
       
        plt.title("Monthly variation of "+variable_name)
        plt.xlabel("Months")
    
        plt.ylabel(variable_name+" (kgN.$ha^{-1}$)")
        plt.savefig(path_out+variable_name+'_Monthly_variation_'+str(table['DATES'].dt.year[0])+'_'+str(table['DATES'].dt.year[len(table['DATES'])-1])+'_'+ ROI+'.png',dpi=300)
       
        plt.show()
        
        
if __name__ == "__main__":
    
    path="../Analysis"
    data=pd.read_csv(path+"/daily_denit_ROI.csv")
   
    average_plot(data,'Denitrification',path,'ROI','tomato')
    
    ## fast plots
    make_ancplot('../../../runs/test/out/anc/SDM_anc.npz')
    make_forcplot('../../../runs/test/out/forc/SDM_forc_20121111T000000.npz')
    make_denitplot('../out/deni/SDM_out_20110125T000000.npz')
    data=np.load('../out/deni/SDM_out_20110125T000000.npz')
    make_map(data['lat'],data['lon'],data['R_NO3'],'R_NO3.png','R_NO3',0.0,data['R_NO3'].max())
    make_denitplot('../deni.npy')
#     # fancy maps
    make_ancmap('../../../runs/test/out/anc/SDM_anc.npz')


