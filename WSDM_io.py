#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#======================================================================================#
#                                   WSDM                                                #
#======================================================================================#
"""WSDM_io : Wetland Soils Denitrification Model input / output script."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#



from xml.etree import ElementTree
from scipy.io import netcdf
import numpy as np
import scipy.interpolate
import glob
import os



def readXML(xmlFile):
    """Read and parse main xml input parameter file.

    Parameters
    ----------
    xmlFile : string
        Path to the input xml file.

    Returns
    -------
    params : dict
        Dictionary of input variables.
    """
    params={}
    with open(xmlFile, 'rt') as f:
        tree = ElementTree.parse(f)
    
    for node in tree.iter():
        
        if not node.text:
            text = "none"
        else:
            text = node.text
            params[node.tag] = text.split(',')
        try:
                params[node.tag]=list(map(int,params[node.tag]) )
        except:
            try:
                params[node.tag]=list(map(float,params[node.tag]) )
            except:
                pass
        params.pop('params', None)
        params.pop('\n', None)
    return params

def read_ecmwf(filename, soil_layer, Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """Read longitudes, latitudes and Skin temperature from AUX_ECMWF file.

    Parameters
    ----------
    filename : string
        path to file.
    soil_layer : int
        choice of soil layer number(1,2,3).
    Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.

    Returns
    -------
    lon : 1D array
        longitudes vector.
    lat : 1D array
        latitudes vector.
    Tsoil : 2D array
        Soil temperature for selected layer in the seected box.
    """
    nc  = netcdf.netcdf_file(filename,'r')
    lon = nc.variables['lon'][Ilonmin:Ilonmax].copy()
    lat = nc.variables['lat'][Ilatmin:Ilatmax].copy()
    Tsoil = nc.variables['Soil_Temperature'][0,Ilatmin:Ilatmax,Ilonmin:Ilonmax,soil_layer].copy()
    nc.close()
    Tsoil=Tsoil -273.15
     
    return lon, lat, Tsoil

def read_SM(filename, variable,Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """Read longitudes, latitudes and Soil moisture.

    Parameters
    ----------
    filename : string
        path to file.
    variable : int
        Soil moisture layer
    Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.

    Returns
    -------
    lon : 1D array
        longitudes vector.
    lat : 1D array
        latitudes vector.
    SM : 2D array
        Soil moisture in the selected box.
    """
    nc  = netcdf.netcdf_file(filename,'r')
    lon = nc.variables['lon'][Ilonmin:Ilonmax].copy()
    lat = nc.variables['lat'][Ilatmin:Ilatmax].copy()
    SM = nc.variables[variable][Ilatmin:Ilatmax,Ilonmin:Ilonmax].copy()
    
    nc.close()
    
    return lon, lat,SM


def read_SMmax(filename, variable,Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """Read maximum soil moisture.

    Parameters
    ----------
    filename : string
        path to file.
    variable : int
        Maximum soil moisture layer
    Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.

    Returns
    -------
    SMmax : 2D array
        Maximum Soil moisture in the selected box..
    """
    nc  = netcdf.netcdf_file(filename,'r')
    if variable=='SM1':
        SMmax = nc.variables['SM1max'][Ilatmin:Ilatmax,Ilonmin:Ilonmax].copy()
    elif variable=='SM2':
        SMmax = nc.variables['SM2max'][Ilatmin:Ilatmax,Ilonmin:Ilonmax].copy()
    else:
        print('SM variable for max not given')
    nc.close()
    SMmax=np.flipud(SMmax)
    return SMmax


def read_EASE_AUX_LANDCOVER(filename, Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """Read longitudes, latitudes for a given ROI.
    
    Parameters
    ----------
    filename : string
        path to file.
    Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.

    Returns
    -------
    lon : 1D array
        longitudes vector.
    lat : 1D array
        latitudes vector.
    land_sea : 2D array
        land or sea in the selected box.
    """
    nc  = netcdf.netcdf_file(filename,'r')
    lon = nc.variables['lon'][Ilonmin:Ilonmax].copy()
    lat = nc.variables['lat'][Ilatmin:Ilatmax].copy()
    land_sea = nc.variables['Land_Sea_Mask'][Ilatmin:Ilatmax,Ilonmin:Ilonmax].copy()
    nc.close()
    return lon, lat, land_sea
    
def get_lon_lat_vectors(filename):
    """Get longitude and latitude.

    Parameters
    ----------
    filename : string
        path to file.

    Returns
    -------
    lon : 1D array
        longitudes vector.
    lat : 1D array
        latitudes vector.

    """
    nc  = netcdf.netcdf_file(filename,'r')
    lon = nc.variables['lon'][:].copy()
    lat = nc.variables['lat'][:].copy()
    nc.close()
    return lon , lat


    
def get_ROI_coord_indexs(lon, lat, lonmin, lonmax, latmin, latmax):
    """Get the min and max indexes for the region of interest.
    
    Parameters
    ----------
    lon : 1D array
        longitudes vector.
    lat : 1D array
        latitudes vector.
    lonmin : int
        Min value of longitude box.
    lonmax :int
        Max value of longitude box.
    latmin : int
        Min value of latitude box.
    latmax : int
        Max value of latitude box.

    Returns
    -------
    idx_lonmin : int
        Index of the min longitude box.
    idx_lonmax : int
        Index of the max longitude box..
    idx_latmin : int
        Index of the min latitude box.
    idx_latmax : int
        Index of the max latitude box.
    """
    idx_lonmin = find_nearest_idx(lon, lonmin)
    idx_lonmax = find_nearest_idx(lon, lonmax)
    idx_latmin = find_nearest_idx(lat, latmin)
    idx_latmax = find_nearest_idx(lat, latmax)
    return idx_lonmin, idx_lonmax, idx_latmin, idx_latmax

def find_nearest_idx(array, value):
    """Find index.

    Parameters
    ----------
    array : 1D array
       lat/lon  arrays
    value : 1D array
       lat/lon values

    Returns
    -------
    idx : int
       index.
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def match_index(lon,lat,lon_wise,lat_wise):
    """Harmonization of latitude and longitude grids.

    Parameters
    ----------
    lon : int
       longitude value from Soil moisture data.
    lat : int
        latitude value from Soil moisture data.
    lon_wise : int
        longitude value from soil data.
    lat_wise : int
        Latitude value from soil data.

    Returns
    -------
    idlon : int
       id of longitude.
    idlat : int
       id of latitude.
    """
    temp_lon=np.empty([len(lon_wise)])
    temp_lat=np.empty([len(lat_wise)])
    
    for i_lon in range(len(lon_wise)):            
            temp_lon[i_lon]=find_nearest_idx(lon, lon_wise[i_lon])
                    
    for i_lat in range(len(lat_wise)):
            temp_lat[i_lat]=find_nearest_idx(lat, lat_wise[i_lat])
    
    
    idlat, idlon = np.meshgrid(temp_lat, temp_lon, sparse=False, indexing='ij')
    return idlon,idlat        

def read_soilprop(soil_prop_dir,Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """ Read longitudes, latitudes and soil properties for a given ROI.
    
    Parameters
    ----------
    soil_prop_dir : string
        path to file.
    Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.

    Returns
    -------
    lon : 1D array
        longitude.
    lat : 1D array
        latitude.
    Bulk :  2D array
        Bulk density.
    OrgC : 2D array
       Organic Carbon content.
    CLPC : 2D array
        Clay percentage.
    CNrt :2D array
      C:N ratio.
    TCEQ : 2D array
        Total carbonate equivalent.
    SDTO :2D array
        Sand content.
    """
    nc  = netcdf.netcdf_file(os.path.join(soil_prop_dir,'BULK.nc'),'r')
    lon = nc.variables['lon'][Ilonmin:Ilonmax].copy()
    lat = nc.variables['lat'][Ilatmax:Ilatmin].copy()
    Bulk = nc.variables['BULK_1.tif'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
    nc.close()
    
    nc  = netcdf.netcdf_file(os.path.join(soil_prop_dir,'ORGC.nc'),'r')
    OrgC = nc.variables['ORGC_1.tif'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
    nc.close()

    nc  = netcdf.netcdf_file(os.path.join(soil_prop_dir,'CLPC.nc'),'r')
    CLPC = nc.variables['CLPC'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
    nc.close()

    nc  = netcdf.netcdf_file(os.path.join(soil_prop_dir,'CNrt.nc'),'r')
    CNrt = nc.variables['CNrt.tif'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
    nc.close()

    nc  = netcdf.netcdf_file(os.path.join(soil_prop_dir,'TCEQ.nc'),'r')
    TCEQ = nc.variables['TCEQ'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
    nc.close()
    
    nc  = netcdf.netcdf_file(os.path.join(soil_prop_dir,'SDTO.nc'),'r')
    SDTO = nc.variables['Sand'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
    nc.close()

    return lon, lat, Bulk,OrgC,CLPC,CNrt,TCEQ,SDTO

def read_wet_typo(file_wetlands,Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """Read longitudes, latitudes and wetlands typology for a given ROI.
    
    Parameters
    ----------
    file_wetlands : string
        path to file.
    Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.

    Returns
    -------
     lon : 1D array
        longitudes vector.
      
    lat : 1D array
        latitudes vector.
    wetlands : 2D array
        wetland distribution
    """
    nc  = netcdf.netcdf_file(file_wetlands,'r')
    lon = nc.variables['lon'][Ilonmin:Ilonmax].copy()
    lat = nc.variables['lat'][Ilatmax:Ilatmin].copy()
    wet_typo = nc.variables['land_use'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
    nc.close()
    return lon,lat,wet_typo

def read_land_class(file_landclass,Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """Read longitudes, latitudes and landuse for a given ROI.
    
    Parameters
    ----------
    file_landclass : string
        path to file. 
    Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.
    Returns
    -------
    lon :  1D array
        longitudes vector.
    lat : 1D array
        latitudes vector.
    land_class :  2D array
        landclass distribution
    """
    nc  = netcdf.netcdf_file(file_landclass,'r')
    lon = nc.variables['lon'][Ilonmin:Ilonmax].copy()
    lat = nc.variables['lat'][Ilatmax:Ilatmin].copy()
    land_class = nc.variables['land_class'][Ilatmax:Ilatmin,Ilonmin:Ilonmax].copy()
 
    nc.close()
    return lon,lat,land_class


def read_EASE_wetlands(filename, Ilonmin, Ilonmax, Ilatmin, Ilatmax):
    """Read longitudes, latitudes and soil moisture for a given ROI.

    Parameters
    ----------
    filename : string
        path to file.
     Ilonmin : int
        Index of the min longitude box.
    Ilonmax : int
        Index of the max longitude box.
    Ilatmin : int
        Index of the min latitude box.
    Ilatmax : int
        Index of the max latitude box.

    Returns
    -------
    lon : 1D array
        longitudes vector.
      
    lat : 1D array
        latitudes vector.
    wetlands : 2D array
        wetland distribution
    """
    nc  = netcdf.netcdf_file(filename,'r')
    lon = nc.variables['lon'][Ilonmin:Ilonmax].copy()
    lat = nc.variables['lat'][Ilatmin:Ilatmax].copy()
    wetlands = nc.variables['wetlands'][Ilatmin:Ilatmax,Ilonmin:Ilonmax].copy() 
    nc.close()
    return lon, lat, wetlands
  

def find_time(SM_files_all_list,start_d,end_d):
    """Find a period of time of the total simulation.

    Parameters
    ----------
    SM_files_all_list : list
        soil moisture values.
    start_d : int
        start date.
    end_d : TYPE
        end date.

    Returns
    -------
    SM_files_list : list
        selection of soil moisture values for a specific period of time.
    """
    SM_files_list=[]
    str_date=int(start_d[0][6:10]+start_d[0][3:5]+start_d[0][0:2])
    end_date=int(end_d[0][6:10]+end_d[0][3:5]+end_d[0][0:2])
    for i_file in range(len(SM_files_all_list)):
        print(i_file)
        cur_file=SM_files_all_list[i_file]
        idate=int(cur_file[-48:-40])
        if idate >= str_date  and idate <= end_date:
            SM_files_list.append(cur_file) 
    return SM_files_list

def find_time_deni(deni_all_list,start_d,end_d):
    """Select a sublist of denitrifation values.

    Parameters
    ----------
    deni_all_list :list
        output of denitrification values .
    start_d : int
       start date.
    end_d : int
        end date.

    Returns
    -------
    deni_files_list : list
        selection of denitrification values for a specific period of time.
    """
    deni_files_list=[]
    str_date=int(start_d[0][6:10]+start_d[0][3:5]+start_d[0][0:2])
    end_date=int(end_d[0][6:10]+end_d[0][3:5]+end_d[0][0:2])
    for i_file in range(len(deni_all_list)):
        print(i_file)
        cur_file=deni_all_list[i_file]
        idate=int(cur_file[-19:-11])
        if idate >= str_date  and idate <= end_date:
            deni_files_list.append(cur_file) 
    return deni_files_list


if __name__ == "__main__":
    
     filename = ('../data/wetlands/wetlands_mask.nc')
     [lon_w, lat_w,ease_wetland]=read_EASE_wetlands(filename, 0,43201,0, 16753)

     [lon_ease_mesh_masked,lat_ease_mesh_masked]=np.meshgrid(lon_w,lat_w)


     lat_ease_mesh_masked_w=lat_ease_mesh_masked[np.where(ease_wetland==1)]
     lon_ease_mesh_masked_w=lon_ease_mesh_masked[np.where(ease_wetland==1)]
    
    
     os.chdir('../data/wetlands') 
     np.save('./wetlands_mask.npz',ease_wetland,allow_pickle=True)



