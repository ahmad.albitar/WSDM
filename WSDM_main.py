#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#======================================================================================#
#                                   WSDM                                                #
#======================================================================================#
"""WSDM_main : Wetland Soils Denitrification Model main script."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#



## import modules and libraires
import os
import glob
import time
import numpy as np
import scipy.interpolate
import shutil

##############################
import WSDM_io
import WSDM_model
import WSDM_tools
import WSDM_plots
import WSDM_post



info_message=[]
warning_message_ecmwf=list()
##### 0. Initialize simulation  ###############################################
message=("0. Initialize simulation")
info_message=WSDM_tools.log_info_message(info_message,message)

##### 0.1 generate folders ###################################################
message=("0.1 generate folders ")
info_message=WSDM_tools.log_info_message(info_message,message)

#clean dirs
cwd=os.getcwd()
cwd


runs_path="../SDM/runs/ROI" # ==> should be defined by user

param_path=os.path.join(runs_path,"inp","params.xml")
outpath   =os.path.join(runs_path,'out')
logpath   =os.path.join(runs_path,'log')
ease_lat_lon_path=os.path.join(runs_path,"inp","inp_EASE25v2_lon_lat.nc") # add to params

shutil.rmtree(outpath, ignore_errors=True)
shutil.rmtree(logpath, ignore_errors=True)

try:
    os.makedirs(outpath)
    os.makedirs(os.path.join(outpath,'deni'))
    os.makedirs(os.path.join(outpath,'forc'))
    os.makedirs(os.path.join(outpath,'anc'))
    os.makedirs(logpath)
except:
    print('[warning] unable to make outdirs: check if they already exist')



###### 0.2 read params file ###################################################
message=("0.2 read params file ")
info_message=WSDM_tools.log_info_message(info_message,message)
params = SDM_io.readXML(param_path)
print('Start /end of processing:',params['start_date'][0],'-->',params['end_date'][0])
print("ROI: region of interest:",params['lonmin'][0],params['lonmax'][0],params['latmin'][0],params['latmax'][0])


###### 1. Read Static Inputs ##################################################
message=("1. Read Static Inputs")
info_message=WSDM_tools.log_info_message(info_message,message)


##### 1.1 Read soil data for ROI ##############################################
message=("1.1 Read soil data for ROI")
info_message=WSDM_tools.log_info_message(info_message,message)


[LON_wise, LAT_wise] = WSDM_io.get_lon_lat_vectors(os.path.join(params['soil_prop_dir'][0],'BULK.nc'))
[idx_lonmin_wise, idx_lonmax_wise, idx_latmin_wise, idx_latmax_wise]= SDM_io.get_ROI_coord_indexs(LON_wise, LAT_wise, params['lonmin'][0], params['lonmax'][0],params['latmin'][0], params['latmax'][0])
[lon_wise, lat_wise, Bulk,OrgC,CLPC,CNrt,TCEQ,SDTO] = WSDM_io.read_soilprop(params['soil_prop_dir'][0],idx_lonmin_wise, idx_lonmax_wise, idx_latmin_wise, idx_latmax_wise)


##### 1.2 landcover classes assing kOC
message=("1.2 Read landcover class for ROI")
info_message=WSDM_tools.log_info_message(info_message,message)
[lon_wise, lat_wise, land_class] = WSDM_io.read_land_class(params['land_class_file'][0],idx_lonmin_wise, idx_lonmax_wise, idx_latmin_wise, idx_latmax_wise)


##### 2. generate grid ########################################################
message=("2. generate grid")
info_message=WSDM_tools.log_info_message(info_message,message)



##### 2.1 generate new grid for ROI  ##########################################
message=("2.1 generate new grid for ROI")
info_message=WSDM_tools.log_info_message(info_message,message)
if (params['res_reduc'][0]) != 1:
    lon_grid=lon_wise[::params['res_reduc'][0]]
    lat_grid=lat_wise[::params['res_reduc'][0]]
else:
    lon_grid=lon_wise
    lat_grid=lat_wise
[lon_grid_mesh,lat_grid_mesh]=np.meshgrid(lon_grid,lat_grid)



##### 2.2 apply new grid to soil data  ########################################
message=("2.2 apply new grid to soil data")
info_message=WSDM_tools.log_info_message(info_message,message)

[Bulk_int,OrgC_int,CLPC_int,CNrt_int,TCEQ_int]=WSDM_tools.resample_soil(Bulk,OrgC,CLPC,CNrt,TCEQ,params['res_reduc'][0])
del Bulk,OrgC,CLPC,CNrt,TCEQ,lon_wise,lat_wise



##### 2.3 generate new grid for KOC  ########################################
message=("2.3 generate new grid for KOC")
info_message=WSDM_tools.log_info_message(info_message,message)

[KOC_opt_int,land_class_int]=WSDM_tools.generate_KOC(land_class,params)
mask=np.where(KOC_opt_int == 0.0)



##### 2.4 generate ease index and masks   #####################################
message=("2.4 generate ease index and masks ")
info_message=WSDM_tools.log_info_message(info_message,message)


[lon, lat] = WSDM_io.get_lon_lat_vectors(ease_lat_lon_path)
[idx_lonmin, idx_lonmax, idx_latmin, idx_latmax]= WSDM_io.get_ROI_coord_indexs(lon, lat, params['lonmin'][0], params['lonmax'][0],params['latmin'][0], params['latmax'][0])
[lon, lat,ease_land_sea]= WSDM_io.read_EASE_AUX_LANDCOVER(params['ease_landcover'][0], idx_lonmin, idx_lonmax, idx_latmin, idx_latmax)

[lon_ease_mesh,lat_ease_mesh]=np.meshgrid(lon,lat)
lat_ease_mesh_masked=lat_ease_mesh[np.where(ease_land_sea>0)]
lon_ease_mesh_masked=lon_ease_mesh[np.where(ease_land_sea>0)]


###### Mask for SMOS_max, Temperature and Landcover
message=("2.3.1 SMOS_max MASK, Temperature and landcover")

SMmax_file=np.load('data/SM2hrmax.npy')

SM2max_hr=SMmax_file[idx_latmax_wise:idx_latmin_wise,idx_lonmin_wise:idx_lonmax_wise] 


###### Mask for Temperature


Tsoil= WSDM_io.read_ecmwf(params['ecmwf_dir'][0]+os.sep+'2010'+ os.sep+'SM_RE04_AUX_CDFECD_20100113T000000_20100113T235959_300_001_7.DBL',params['selected_Tsoil_layer'][0],idx_lonmin, idx_lonmax, idx_latmin, idx_latmax)
Tsoil_int=scipy.interpolate.griddata((lat_ease_mesh_masked.flatten(),lon_ease_mesh_masked.flatten()),SM2max_hr[np.where(ease_land_sea>0)].flatten() , (lat_grid_mesh,lon_grid_mesh),method='linear')
Tsoil_int[mask]=np.nan

###### Mask for Landcover
if params['save_anc'][0]==1:
   message=("2.5 saving anc file before loop")
   info_message=WSDM_tools.log_info_message(info_message,message)
   out_filename = 'SDM_anc'
   np.savez(os.path.join(outpath,'anc',out_filename), lon=np.float16(lon_grid),lat=np.float16(lat_grid),KOC_opt=np.float16(KOC_opt_int),land_class=np.float16(land_class_int),Bulk=np.float16(Bulk_int),OrgC=np.float16(OrgC_int),CLPC=np.float16(CLPC_int),CNrt=np.float16(CNrt_int),TCEQ=np.float16(TCEQ_int),SMmax=np.float16(SM2max_hr))


 
##### 3. main loop ############################################################
message=("3.0 main loop")
info_message=WSDM_tools.log_info_message(info_message,message)



SM_files_all_list=sorted(glob.glob(params['smos_SMRZ_dir'][0]+os.sep+'*'+os.sep+'*.nc'))
## Extract files list based on simulation params
SM_files_list=WSDM_io.find_time(SM_files_all_list,params['start_date'],params['end_date'])


Tstart=time.time()

## initialiez Nitrate stock
NO3_t1=np.zeros(np.shape(Bulk_int))
NO3_t2=np.zeros(np.shape(Bulk_int))
R_NO3_tot=np.zeros(np.shape(Bulk_int))
NO3_act_tot=np.zeros(np.shape(Bulk_int))
NO3_budget=np.zeros(np.shape(Bulk_int))

#i_file= is the time that will be analized as first
i_file=0

for i_file in range(len(SM_files_list)): #len(SM_files_list)):

    if i_file == 0:
     # first file ==> get initial time
        time1=0.0
        NO3_budget
    if i_file == 10: # give estimates for the run
        T10=time.time()
        message=('Expected time to process all files: ' +  str((T10-Tstart)*len(SM_files_list)/10.0/60.0)+ ' min')
        info_message=WSDM_tools.log_info_message(info_message,message)


    ### 3.1.read files and interpolate
    ### 3.1.a- read, interp and apply mask to SM product
    smos_SMRZ_filepath = SM_files_list [i_file]
    path_split=smos_SMRZ_filepath.split(os.sep)
    message='processing file :' + str(i_file+1) + '/' + str(len(SM_files_list)) +' '+ path_split[-1]
    info_message=WSDM_tools.log_info_message(info_message,message)

    # get time info
    time2=time1+1 
    print(time2)

    [lon, lat, SM] = WSDM_io.read_SM(smos_SMRZ_filepath,params['selected_SM'][0],idx_lonmin, idx_lonmax, idx_latmin, idx_latmax)
    SM_int=scipy.interpolate.griddata((lat_ease_mesh_masked.flatten(),lon_ease_mesh_masked.flatten()),SM[np.where(ease_land_sea>0)].flatten() , (lat_grid_mesh,lon_grid_mesh),method='linear')
    SM_int[mask]=np.nan

    ### 3.1.b- read, interp and apply mask to ECMWF product
    try:
        name_ecmwf_temp='*'+path_split[-1][19:34]+'*.DBL'
        smos_ecmwf_filepath=glob.glob(os.path.join(params['ecmwf_dir'][0],path_split[-2],name_ecmwf_temp))
        [lon, lat, Tsoil] = WSDM_io.read_ecmwf(smos_ecmwf_filepath[0],params['selected_Tsoil_layer'][0],idx_lonmin, idx_lonmax, idx_latmin, idx_latmax)
    #interp temp
        Tsoil_int=scipy.interpolate.griddata((lat_ease_mesh_masked.flatten(),lon_ease_mesh_masked.flatten()),Tsoil[np.where(ease_land_sea>0)].flatten() , (lat_grid_mesh,lon_grid_mesh),method='linear')
        Tsoil_int[mask]=np.nan
    except:
        message='[Warning] ECMWF file not available for : ' + SM_files_list[i_file]
        info_message=WSDM_tools.log_info_message(info_message,message)
        warning_message_ecmwf.append(SM_files_list[i_file])


    ### 3.2. run physical model modules on masked pixels only

    # 3.2.1- compute nitrification 
    NO3_act = SDM_model.nitrification(OrgC_int,CNrt_int,Bulk_int,TCEQ_int, CLPC_int,SM_int,SM2max_hr,Tsoil_int,params['Th_SM_nit'][0])
    
    if i_file == 0:
     # first file ==> get initial time
        NO3_budget = NO3_act
        
    # 3.2.2 - compute denitrification
    R_NO3 = SDM_model.denit_rate(KOC_opt_int,Bulk_int,OrgC_int,NO3_budget,params ['KNO3'][0],params['redfr'][0],SM_int,SM2max_hr,Tsoil_int,params['Temp_opt'][0],params['Th_SM_denit'][0], params['depth1'][0])

    #  Calculate denitrification total over the time period
    NO3_act_tot=np.nan_to_num(NO3_act_tot+NO3_act)      
    R_NO3_tot= np.nan_to_num(R_NO3_tot+R_NO3)  

    # 3.2.3 update stock and time
    if i_file == 0:
     # first file ==> get initial NO3_budget=NO3_act
        NO3_t1= NO3_act
    NO3_t2 = SDM_model.NO3_budget(NO3_t1,R_NO3,NO3_act,params['dt'][0])
    R_NO3 = SDM_model.denit_update(R_NO3, NO3_t1, NO3_act)
 
    time1=time2
    NO3_t1 = NO3_t2
    
       
    ### 3.3 save outputs
    ### 3.3.1 remap outputs
    NO3_act_int = WSDM_tools.remap(NO3_act,mask,np.shape(Bulk_int))
    NO3_act[mask]=np.nan
    NO3_act_int = NO3_act
    K_OC_int   = WSDM_tools.remap(KOC_opt_int,mask,np.shape(Bulk_int))
    R_NO3_int  = WSDM_tools.remap(R_NO3,mask,np.shape(Bulk_int))
    R_NO3[mask]=np.nan
    R_NO3_int  = R_NO3 
    NO3_budget    = WSDM_tools.remap(NO3_t2,mask,np.shape(Bulk_int))
    
    NO3_budget    = NO3_t2
    


    # if i_file % 2 == 0:
        
        ### 3.3.2 save outputs    
    out_filename = 'SDM_out_' + path_split[-1][19:34]
       
    np.savez_compressed(os.path.join(outpath,'deni',out_filename), lon=np.float16(lon_grid),lat=np.float16(lat_grid), NO3_act=np.float16(NO3_act),R_NO3=np.float16(R_NO3),R_NO3_tot=np.float16(R_NO3_tot))
 
    if params['save_forc'][0]==1:

        out_filename = 'SDM_forc_' + path_split[-1][19:34]
        np.savez_compressed(os.path.join(outpath,'forc',out_filename), lon=np.float16(lon_grid),lat=np.float16(lat_grid),Tsoil=np.float16(Tsoil_int),SM=np.float16(SM_int))  

        
### end of main loop
message=('End of main loop')
info_message=WSDM_tools.log_info_message(info_message,message)

### 4. save log files

message=('4. Save log files')
info_message=WSDM_tools.log_info_message(info_message,message)

log__filename =('SDM_log')
np.save(os.path.join(logpath,'SDM_log'),info_message)
np.save(os.path.join(logpath,'SDM_ecmwf'),warning_message_ecmwf)


###############################################################################
### End of simulation
message=('############## End of simulation ##################### \n check: \n' + logpath +' \n ' + outpath  )
info_message=WSDM_tools.log_info_message(info_message,message)
###############################################################################


if __name__ == "__main__":

    WSDM_plots.make_ancplot( os.path.join(outpath,'anc','SDM_anc.npz'))
    WSDM_plots.make_forcplot( os.path.join(outpath,'forc','SDM_forc_20100123T000000.npz'))
    WSDM_plots.make_denitplot( os.path.join(outpath,'deni','SDM_out_20110123T000000.npz'))
    WSDM_plots.make_denitplot( os.path.join(outpath,'deni','SDM_out_20100123T000000.npz'))
#
    if params['extract_TS_pid'][0]==1:
        pid_name,pid_lat,pid_lon=WSDM_post.read_poi_file(params['extract_TS_pid_file'][0])
        ts_deni_dict=WSDM_post.extract_timeseries_deni(runs_path,['NO3_act','R_NO3'],pid_name,pid_lat,pid_lon)
        ts_forc_dict=WSDM_post.extract_timeseries_forc(runs_path,['Tsoil','SM'],pid_name,pid_lat,pid_lon)

    #use these for ploting
        ts_forc_dict['date_vec']
        ts_forc_dict['SM']

    ts_deni_dict['date_vec']
    ts_deni_dict['R_NO3']
