*******************************
WSDM Read me
*******************************
Wetland Soils Denitrification Model is a simple process-based model for estimation of Nitrogen denitrification fluxes in wetland ecosystems.

Read me: Table of content
#########################

1. Description

2. Prerequisites

3. Installation

4. Running the model

5. Modules description

6. References

7. License

1. Description 
##############

WSDM is a parsimoneous physical model that enables the computation of
denitrification processes in wetlands.

The model has been developed by 
Laboratoire écologie fonctionnelle et environnement - ECOLAB (Université de Toulouse, CNRS UT3, INPT)
Centre d'Etudes Spatiales de la Biosphére CESBIO (Université de Toulouse, UPS, CNRS, CNES, IRD, INRAE).

2. Prerequisites 
################

WSDM is implemented in python3 and runs over Windows and Linux based OS

External python packages:
  - numpy (https://numpy.org/) - Numerical Python library, the fundemental scientific python library.
  - matplotlib (https://matplotlib.org/) - 2D plotting library.
  - scipy (https://scipy.org/) - scientific python library 
  - basemap (https://matplotlib.org/basemap/)

Internal python packages: datetime,os,sys, glob, xml.etree, shutil

3. Installation  
################

3.1 Intall python packages
**************************
- Option 1 - use anaconda python package (Linux/Windows):

Install anaconda (https://www.anaconda.com/) then:

::

  conda install matplotlib
  conda install basemap

- Option 2 - install python and packages:
::

  sudo apt-get install python3.6
  pip install numpy
  pip install matplotlib
  pip install scipy
  pip install basemap
  
3.2 Intall python packages
**************************

- Option 1 - Download package - (Linux/Windows)
::

  from framagit WSDM (https://framagit.org/ahmad.albitar/WSDM)
  click dowload (next to clone)

- Option 2 - Use git (install and fetch) - over Linux 

::
  sudo apt-get install git
  mkdir <workdirectory>
  cd <workdirectory>
  git clone https://framagit.org/ahmad.albitar/WSDM.git



4. Running the model
####################
 Step 1. Once the environment is created  add all SDM modules. For this, a Integrated DeveLopment Environment (IDLE) is needed (such as Eclipse, Spyder...) since some adjustments need to be done for each project.
 
 Step 2. Input data directories in the "Params.xml", constructed as an eXtensible Markup Language, (XML). This format is easy to use, and can be open as text file. 
	     This file contains the tag names that clearly define and explain the data. For the SDM there are two files, one that is the ione used for run the main simulation and the second that is used to called the outputs of the SDM and to be analized by region of interest or watershed. 
	     An example of both xml files needed are storaged in https://framagit.org/ahmad.albitar/WSDM/-/blob/master/sample_run/inp
		   
 Step 3. Open WSDM_main and define the run path, in this path, the output folders will be created and the outputs files at daily time step will be stored. The outputs files are in .npz format
 

5. Modules Description
####################### 

The main module of SDM is "WSDM_main". The call graphe of the WSDM is presented below.

.. image:: WSDM_graph.svg
  :width: 900
  :alt: call graph


A brief description of each module is presented below:

- WSDM_main.py : This module is the main driver module.
    - imports the needed modules,
    - initialise the simulation and output dirs based on the input xml file. 
    - Reads data directories of dynamic data, and static data. All the input data is global, if the simulation wants to be run on a specific area, the data is being extracted for a region of interest defined by the user, in latitude and longitude polygon. 
Then Koc is assigned and the simulation is carried out only in the terrestrial territories where Koc is given (i.e. wetlands). 
Once the data is homogenized, then denitrification model loop run at daily scale. Each loop, denitrification, nitrification, and budget, are stored as fluxes in mgN g-1.

- WSDM_io.py : This module is contains all input and output readers.

- WSDM_model.py : This module contains the physicl modules for : Nitrification, Denitrification, Nitrates budget).

- WSDM_tools.py : This module is constituted by functions that help cut the global data to the region of interest, and to homogenize the grid using the soil database as the reference grid. It also assigned the mineralization rates (Koc) to each pixel according to their land class.

- WSDM_post.py : This module allows the extraction of node based time series of denitrification, nitrification, as well as forcing data (temperature, soil moisture). It is possible to extract a specific period, or the whole time series by specific punctual location. The input points example can be found at https://framagit.org/ahmad.albitar/SDM/-/blob/master/sample_run/sample_points.txt

- WSDM_plots.py : This module generates geographically projected maps.

Additional modules: 

- WSDM_analysis_general.py : This module shows some of the analysis that can be done with the diurnal denitrification rates, for example, interannual denitrification mean; annual and monthly mean by Region of Interest (ROI). Total denitrification (N2 + N2O) by area (kg N ha-1), daily, weekly, monthly and annual can be also calculated. These functions; are a first exploratory exploitation of SDM, but the algorithms can be improved in addition to adapted to specific objectives, for more in-depth analysis.

- WSDM_analysis_by_wetland_typology.py : This module allows the user to extract daily results by wetland type and to use a mask to define the area of interest, it can be a whatershed or any other polygone. This module can be used to extract any of the SDM outputs and inputs (i.e. denitrification, nitrification, soil moisture, temperature, ans soil properties).

6. References
##############

Martinez-Espinosa, C., Sauvage, S., Al Bitar, A., & Sanchez-Perez, J. M. (2022). A dynamic model for assessing soil denitrification in large-scale natural wetlands driven by Earth Observations. Environmental Modelling & Software, 105557. https://doi.org/10.1016/j.envsoft.2022.105557

Martínez-Espinosa, C., Sauvage, S., Al Bitar, A., Green, P. A., Vörösmarty, C. J., & Sánchez-Pérez, J. M. (2021). Denitrification in wetlands: A review towards a quantification at global scale. Science of the total environment, 754, 142398. https://doi.org/10.1016/j.scitotenv.2020.142398

7. License
##############

Credits = "C. Martinez-Espinosa, S. Sauvage, A. Al Bitar, J. M. Sanchez-Perez"
Authors= "A. Al Bitar & C. Martinez-Espinosa"
copyright = "CNRS-ECOLAB-CESBIO"
license = "GPL"
version = "0.1"
